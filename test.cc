
#include <iostream>
#include "xdgbds.h"

int main(int argc, char* argv[])
{
        std::cout << "XDG_DATA_HOME\n";
        std::cout << "  " << xdg::data::home() << "\n";

        std::cout << "XDG_DATA_DIRS\n";
        for (auto d : xdg::data::dirs())
                std::cout << "  " << d << "\n";

        std::cout << "XDG_CONFIG_HOME\n";
        std::cout << "  " << xdg::config::home() << "\n";

        std::cout << "XDG_CONFIG_DIRS\n";
        for (auto d : xdg::config::dirs())
                std::cout << "  " << d << "\n";

        std::cout << "XDG_STATE_HOME\n";
        std::cout << "  " << xdg::state::home() << "\n";

        std::cout << "XDG_CACHE_HOME\n";
        std::cout << "  " << xdg::cache::home() << "\n";

        std::cout << "XDG_RUNTIME_DIR\n";
        std::cout << "  " << xdg::runtime::home() << "\n";

        return 0;
}
