
#ifndef SRC_XDG_BASE_DIRS_H_
#define SRC_XDG_BASE_DIRS_H_

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <filesystem>
#include <vector>
#include <ranges>

// Implements the XDG Base Directory Specification to locate files. 

namespace xdg {

#ifndef _WIN32
static constexpr char separator = ':';
#else
static constexpr char separator = ';';
#endif

template<class Traits> struct Directories_impl {
 private:
        Traits traits_;
        std::vector<std::filesystem::path> dirs_;

 public:
        Directories_impl(const Traits& traits = Traits()) : traits_{traits}
        {
                dirs_.push_back(traits_.home());
                dirs_.back().make_preferred();

                auto d = traits_.dirs();
                if (!d.empty()) {
                        const char *p = d.data();
                        for (;;) {
                                const char *n = p;
                                while (*n != 0 && *n != separator) n++;
                                dirs_.push_back(std::string(p, n));
                                dirs_.back().make_preferred();
                                if (*n == 0) break;
                                p = n + 1;
                        }
                }
        }

        const auto& home() const { return dirs_[0]; }
        const auto dirs() const { return dirs_ | std::views::drop(1); }

        auto find(const std::filesystem::path& path) const
        {
                for (auto& dir : dirs_) {
                        auto r = dir / path;
                        r.make_preferred();
                        if (std::filesystem::exists(r))
                                return r;
                }

                fprintf(stderr, "%s: no such file or directory\n", path.string().data());
                for (auto& dir : dirs_)
                        fprintf(stderr, "- tried %s\n", (dir / path).make_preferred().string().data());

                return std::filesystem::path{};
        }
};

template<class Traits> struct Directories {
 private:
        static Directories_impl<Traits>& impl()
        {
                static Directories_impl<Traits> instance;
                return instance;
        }

 public:
        static decltype(auto) home() { return impl().home(); }
        static decltype(auto) dirs() { return impl().dirs(); }
        static decltype(auto) find(const std::filesystem::path& path) { return impl().find(path); }
};

static std::string getenv_safe(const char* name)
{
        auto r = std::getenv(name);
        if (r && *r) return r;
        fprintf(stderr, "fatal: unset environment variable '%s'\n", name);
        abort();
}

struct Data {
        std::string home() const
        {
                auto r = std::getenv("XDG_DATA_HOME");
                if (r && *r) return r;
#               ifndef _WIN32
                return getenv_safe("HOME") + "/.local/share";
#               else
                return getenv_safe("APPDATA");
#               endif
        }

        std::string dirs() const
        {
                auto r = std::getenv("XDG_DATA_DIRS");
                if (r && *r) return r;
#               ifndef _WIN32
                return "/usr/local/share:/usr/share";
#               else
                return "C:\\ProgramData";
#               endif
        }
};

using data = Directories<Data>;

struct Config {
        std::string home() const
        {
                auto r = std::getenv("XDG_CONFIG_HOME");
                if (r && *r) return r;
#               ifndef _WIN32
                return getenv_safe("HOME") + "/.config";
#               else
                return getenv_safe("APPDATA");
#               endif
        }

        std::string dirs() const
        {
                auto r = std::getenv("XDG_CONFIG_DIRS");
                if (r && *r) return r;
#               ifndef _WIN32
                return "/etc/xdg";
#               else
                return "C:\\ProgramData";
#               endif
        }
};

using config = Directories<Config>;

struct State {
        std::string home() const
        {
                auto r = std::getenv("XDG_STATE_HOME");
                if (r && *r) return r;
#               ifndef _WIN32
                return getenv_safe("HOME") + "/.local/state";
#               else
                return getenv_safe("APPDATA");
#               endif
        }

        std::string dirs() const { return ""; }
};

using state = Directories<State>;

struct Cache {
        std::string home() const
        {
                auto r = std::getenv("XDG_CACHE_HOME");
                if (r && *r) return r;
#               ifndef _WIN32
                return getenv_safe("HOME") + "/.cache";
#               else
                return getenv_safe("APPDATA");
#               endif
        }

        std::string dirs() const { return ""; }
};

using cache = Directories<Cache>;

struct Runtime {
        std::string home() const
        {
                auto r = std::getenv("XDG_RUNTIME_DIR");
                if (r && *r) return r;
#               ifndef _WIN32
                return "/tmp";
#               else
                return getenv_safe("TEMP");
#               endif
        }

        std::string dirs() const { return ""; }
};

using runtime = Directories<Runtime>;


}; // namespace xdg

#endif // SRC_XDG_BASE_DIRS_H_
