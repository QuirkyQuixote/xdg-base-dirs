XDG Base Directories for C++20
==============================

A simple, header-only library to locate files according to tho XDG Base
Directory Specification for C++20.

According to
https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

Usage
-----

Include the `xdgbds.h` file, use the contained functions, see the file
itself for details.

The `xdg::FOO::home()` functions return one path.

The `xdg::FOO::dirs()` functions return a vector of paths.

The `xdg::FOO::find(path)` functions find the first file with the given path
that exists under `xdg::FOO::home()` and `xdg::FOO::dirs()` (in that order) and
return it's full absolute path; if no such file exists, they return an empty
path object and print an appropriate error to stderr.

| function              | value                 | default |
|-----------------------|-----------------------|-----------|
| xdg::data::home()     | $(XDG_DATA_HOME)      | $(HOME)/.local/share |
| xdg::data::dirs()     | $(XDG_DATA_DIRS)      | /usr/local/share:/usr/share |
| xdg::config::home()   | $(XDG_CONFIG_HOME)    | $(HOME)/.config |
| xdg::config::dirs()   | $(XDG_CONFIG_DIRS)    | /etc/xdg |
| xdg::state::home()    | $(XDG_STATE_HOME)     | $(HOME)/.local/state |
| xdg::cache::home()    | $(XDG_CACHE_HOME)     | $(HOME)/.cache |
| xdg::runtime::home()  | $(XDG_RUNTIME_DIR)    | /tmp |

The library provides a windows alternative for these paths, where the defaults
for the xdg variables are different

| function              | value                 | default |
|-----------------------|-----------------------|-----------|
| xdg::data::home()     | %XDG_DATA_HOME%       | %APPDATA% |
| xdg::data::dirs()     | %XDG_DATA_DIRS%       | C:\ProgramData |
| xdg::config::home()   | %XDG_CONFIG_HOME%     | %APPDATA% |
| xdg::config::dirs()   | %XDG_CONFIG_DIRS%     | C:\ProgramData |
| xdg::state::home()    | %XDG_STATE_HOME%      | %APPDATA% |
| xdg::cache::home()    | %XDG_CACHE_HOME%      | %APPDATA% |
| xdg::runtime::home()  | %XDG_RUNTIME_DIR%     | %TEMP% |

